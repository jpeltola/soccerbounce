﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

// Script for UI window where player sends the name to leaderboard
public class NameInputUI : MonoBehaviour
{
    [SerializeField] InputField inputField;

    public void SendToLeaderboard() // Is called by pressing the send button on NameInputWindow
    {
        string name = inputField.text;
        int score = FindObjectOfType<PointCounter>().GetScore();

        FindObjectOfType<Leaderboard>().AddNew(name, score);
    }
}
