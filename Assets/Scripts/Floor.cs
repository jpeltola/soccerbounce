﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;


// Script for lose condition: ball hits the floor
public class Floor : MonoBehaviour
{
    public event Action floorHitAction;

    bool isHit = false;

    private void OnCollisionEnter(Collision collision)
    {
        SoccerBall ball = collision.gameObject.GetComponent<SoccerBall>();

        // if the other collider was a type of ball, invoke the floorHitAction to handle the lose script
        if(ball != null && !isHit)
        {
            isHit = true;
            ball.enabled = false;
            floorHitAction?.Invoke();
        }
    }
}
