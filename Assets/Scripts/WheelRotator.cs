﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


// Simple rotating movement component for car wheels
public class WheelRotator : MonoBehaviour
{
    [SerializeField] float RPS = 240;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        gameObject.transform.Rotate(new Vector3(1, 0, 0), RPS*Time.deltaTime);
    }
}
