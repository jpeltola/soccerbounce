﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


// Hides the gameobject when mouse button is clicked
public class HideOnClick : MonoBehaviour
{


    // Update is called once per frame
    void Update()
    {
        if(Input.GetMouseButtonDown(0))
        {
            gameObject.SetActive(false);
        }
    }
}
