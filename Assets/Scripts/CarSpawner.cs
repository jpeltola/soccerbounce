﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;


// Spawns the cars from left or right side of the screen
public class CarSpawner : MonoBehaviour
{
    [SerializeField] List<GameObject> carPrefabs = new List<GameObject>();

    [SerializeField] Transform leftSpawn;
    [SerializeField] Transform rightSpawn;

    [SerializeField] int pointsToActivate = 20;
    [SerializeField] int pointsToSpawnBuses = 50;
    [SerializeField] int maxSpawnRatePoint = 100;

    [SerializeField]  float minCooldown = 5;
    [SerializeField]  float maxCooldown = 20;


    int carMinIndex = 2; // This is for that the buses wont spawn at the beginning

    //Min and max cooldown will be adjusted as the game proceeds


    float currentPoints = 0;

    float cooldown = 20;
    float timeLastSpawned = 20;

    bool isActive = false;



    private void Start()
    {
        FindObjectOfType<PointCounter>().pointsAchieved += HandlePointGain;
    }

    // Update is called once per frame
    void Update()
    {
        if (!isActive) return; // do nothing if not active
        if(timeLastSpawned >= cooldown)
        {
            Instantiate(carPrefabs[GetRandomCar()],GetRandomSpawnPoint());
            timeLastSpawned = 0;
            
            
        }

        timeLastSpawned += Time.deltaTime;
    }

    private Transform GetRandomSpawnPoint()
    {
        int index = UnityEngine.Random.Range(0, 2);
        if (index == 0) return leftSpawn;
        else return rightSpawn;
    }


    // Get a random cooldown based on current points
    private float GetRandomCooldown()
    {

        if (currentPoints < maxSpawnRatePoint)
        {
            return (minCooldown - maxCooldown) / (maxSpawnRatePoint - pointsToActivate) * (currentPoints - maxSpawnRatePoint) + minCooldown;
        }
        else
        {
            return minCooldown;
        }

    }

    public void Activate()
    {
        isActive = true;
        
    }

    // returns the index of a random car
    private int GetRandomCar()
    {
        return UnityEngine.Random.Range(carMinIndex, carPrefabs.Count);
    }

    // Handle the point gain event
    private void HandlePointGain(int points)
    {
        currentPoints = points;
        cooldown = GetRandomCooldown();
        if (currentPoints >= pointsToSpawnBuses) 
        {
            carMinIndex = 0;
        }
        if (currentPoints >= pointsToActivate && !isActive)
        {
            Activate();
        }
        
    }
}
