﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.UIElements;
using System;


// Counts the total number of bounces and updates to the UI
public class PointCounter : MonoBehaviour
{
    [SerializeField] TextMeshProUGUI pointText;

    int points = 0;

    public event Action<int> pointsAchieved;

    public int GetScore()
    {
        return points;
    }

    private void OnEnable()
    {
        SoccerBall ball = FindObjectOfType<SoccerBall>();
        ball.ballBounced += UpdatePoints;
    }

    private void UpdatePoints()
    {
        if(pointText != null)
        {
            points += 1;
            pointText.SetText(points.ToString());
            pointsAchieved?.Invoke(points);
        }
        
    }
}
