﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


// Component for constant forward speed, and restricted lifetime
public class Car : MonoBehaviour
{
    [SerializeField] float speed = 15;
    // Start is called before the first frame update
    private void Awake()
    {
        Destroy(gameObject, 10);
    }

    // Update is called once per frame
    void Update()
    {
        transform.position += transform.forward * speed * Time.deltaTime; 
    }
}
