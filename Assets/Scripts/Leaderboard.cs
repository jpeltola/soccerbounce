﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using UnityEngine;

public class Leaderboard : MonoBehaviour
{

    [SerializeField] HighScore scorePrefab;

    string saveFile = "save";
    string scoreToken = "_score";
    string nameToken = "_name";

    // Scoredata contains the name and score and implements the IComparable interface so it can be sorted in lists
    class ScoreData: IComparable<ScoreData>
    {
        public string name;
        public int score;

        public int CompareTo(ScoreData other)
        {
            return other.score.CompareTo(score);
        }
    }

    List<ScoreData> leaderBoard = new List<ScoreData>();

    private void Awake()
    {
        Load(); //Try to load save file
        BuildLeaderBoard(); // Build the leaderboard UI
    }

    // Build the LeaderboardUI
    public void BuildLeaderBoard()
    {
        foreach(Transform child in transform)
        {
            Destroy(child.gameObject);
        }
        for (int i = 0; i < leaderBoard.Count;i++)
        {
            HighScore highScoreInstance = Instantiate(scorePrefab, transform);

            highScoreInstance.SetHighScore(i + 1, leaderBoard[i].name, leaderBoard[i].score);
        }
    }

    //Check if the score should be added
    public bool IsHighScore(int score)
    {
        if (leaderBoard.Count < 10) return true;
        foreach(var scoreData in leaderBoard)
        {
            if(scoreData.score < score)
            {
                return true;
            }
        }
        return false;
    }

    // Add new highscore to the leaderboard
    public void AddNew(string name, int score)
    {
        ScoreData scoreData = new ScoreData();
        scoreData.name = name;
        scoreData.score = score;

        leaderBoard.Add(scoreData);
        leaderBoard.Sort(SortByScore);
        if (leaderBoard.Count > 10)
        {
            DeleteLast();
        }
        Save();
        BuildLeaderBoard();
    }

    // Sort scoredata by score
    static int SortByScore(ScoreData scr1,ScoreData scr2)
    {
        return scr1.CompareTo(scr2);
    }

    // Delete the last in leaderboards (this is used when the leaderboard is full)
    private void DeleteLast()
    {
        leaderBoard.RemoveAt(leaderBoard.Count - 1);
    }

    private string GetPathFromSaveFile(string saveFile)
    {
        return Path.Combine(Application.persistentDataPath, saveFile + ".sav");
    }

    // Saves the leaderboard to file
    private void Save()
    {

        string path = GetPathFromSaveFile(saveFile);
        using (FileStream stream = File.Open(path, FileMode.Create))
        {
            BinaryFormatter formatter = new BinaryFormatter();
            Dictionary<string, object> saveData = new Dictionary<string, object>();

            for(int i = 0; i < leaderBoard.Count;i++)
            {
                string saveTokenName = i.ToString() + nameToken;
                string saveTokenScore = i.ToString() + scoreToken;
                saveData[saveTokenScore] = leaderBoard[i].score;
                saveData[saveTokenName] = leaderBoard[i].name;
            }

            formatter.Serialize(stream, saveData);
        }
    }

    // Loads the leaderboard from file
    private void Load()
    {
        string path = GetPathFromSaveFile(saveFile);
        if (!File.Exists(path))
        {
            return;
        }
        using (FileStream stream = File.Open(path, FileMode.Open))
        {
            BinaryFormatter formatter = new BinaryFormatter();
            Dictionary<string, object> saveDataScores = (Dictionary<string, object>)formatter.Deserialize(stream);
            for(int i = 0; i < 10; i ++)
            {
                ScoreData scoreData = new ScoreData();
                string key = i.ToString() + nameToken;
                if (saveDataScores.ContainsKey(key))
                {
                    scoreData.name = (string)saveDataScores[key];
                }
                else return;
                key = i.ToString() + scoreToken;
                if (saveDataScores.ContainsKey(key))
                {
                    scoreData.score = (int)saveDataScores[key];
                }
                else return;
                leaderBoard.Add(scoreData);
            }
        }
        leaderBoard.Sort(SortByScore);
    }
 
}
