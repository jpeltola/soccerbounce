﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;


// Script for the soccerball, bouncing functionality
public class SoccerBall : MonoBehaviour
{
    [SerializeField] float maxRange = 1f;
    [SerializeField] float maxForceStrength = 800f;
    [SerializeField] float minForceStrength = 200f;
    [Range(0,90)]
    [SerializeField] float maxAngle = 45;

    [SerializeField] ParticleSystem clickParticleSystem;

    private Vector3 position;

    public event Action ballBounced;
    // Start is called before the first frame update
    void Start()
    {
        GetComponent<Rigidbody>().Sleep();
    }

    // Update is called once per frame
    void Update()
    {
        if(Input.GetMouseButtonDown(0))
        {
            GetComponent<Rigidbody>().WakeUp();
            position = Camera.main.ScreenToWorldPoint(Input.mousePosition); // Get the world position of the click
            position.z = 0; // Set z = 0 because of 2D
            if (IsInRange())
            {
                Bounce();
            }
        }
    }

    // Launch the ball to calculated direction
    private void Bounce()
    {
        Vector3 forceDirection = CalculateForceDirection();
        float forceMagnitude = CalculateForceFalloff();
        GetComponent<Rigidbody>().AddForceAtPosition(forceDirection * forceMagnitude, position);
        GetComponent<AudioSource>().Play();
        ballBounced?.Invoke();
    }

    //Calculate the falloff of the force due to the distance
    private float CalculateForceFalloff()
    {
        float distance = Vector3.Distance(transform.position, position);
        return maxForceStrength - distance/maxRange*(maxForceStrength - minForceStrength);
    }

    // Calculate the direction the ball is launched 
    Vector3 CalculateForceDirection()
    {
        Vector3 direction = transform.position - position;
        direction.y = Mathf.Abs(direction.y); //prevent downward force
          
        float angle = Mathf.Atan(direction.x / direction.y) * Mathf.Rad2Deg; // Angle between the force and upward vector in degrees
        angle = Mathf.Clamp(angle,-maxAngle,maxAngle);
        
        // Clamp the vector direction
        direction.x = direction.magnitude*Mathf.Sin(angle*Mathf.Deg2Rad); 
        direction.y = direction.magnitude * Mathf.Cos(angle * Mathf.Deg2Rad);

        direction = Vector3.Normalize(direction);
        direction.z = UnityEngine.Random.Range(0.0f, 1.0f); // for gettin rotation around X and Y axis
        return direction;
    }
    
    // Check if the click is close enough to the ball
    bool IsInRange()
    {
        return Vector2.Distance(position, transform.position) < maxRange;
    }

    // Play the sound if hit a wall or floor and speed is high enough
    private void OnCollisionEnter(Collision collision)
    {
        if(GetComponent<Rigidbody>().velocity.magnitude > 0.1)
        {
            GetComponent<AudioSource>().Play();
        }
    }
    
}

