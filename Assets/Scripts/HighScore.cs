﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


// Script for one highscore UI element
public class HighScore : MonoBehaviour
{
    [SerializeField] Text rankText;
    [SerializeField] Text nameText;
    [SerializeField] Text scoreText;

    public void SetHighScore(int rank, string name, int score)
    {
        rankText.text = rank.ToString();

        if (name == "") name = "anon";
        nameText.text = name;

        scoreText.text = score.ToString();
    }

}
