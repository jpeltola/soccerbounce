﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

// Script for the end screen, handles quitting and restarting
public class EndScreen : MonoBehaviour
{
    [SerializeField] GameObject nameInputWindow;
    [SerializeField] GameObject leaderBoardWindow;
    public void Restart()
    {
        gameObject.SetActive(false);
        SceneManager.LoadSceneAsync(SceneManager.GetActiveScene().buildIndex);
    }

    public void QuitGame()
    {
        Application.Quit();
    }

    private void Start()
    {
        FindObjectOfType<Floor>().floorHitAction += OpenEndScreen;
        gameObject.SetActive(false);
    }

    // Opens the end screen, when ball hits the floor. Depending on whether is a highscore, open leaderboards or name input UI
    private void OpenEndScreen()
    {   
        gameObject.SetActive(true);
        leaderBoardWindow.SetActive(true);
        bool isHighScore = FindObjectOfType<Leaderboard>().IsHighScore(FindObjectOfType<PointCounter>().GetScore());

        nameInputWindow.SetActive(isHighScore);
        leaderBoardWindow.SetActive(!isHighScore);
    }

}
